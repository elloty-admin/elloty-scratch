/**
 * @api {get} /scratches/:id/ Request Scratch with selected ID
 * @apiName GetScratchById
 * @apiGroup Scratches
 *
 * @apiParam {Number} id Scratch unique ID.
 *
 * @apiUse ScratchResult
 *
 * @apiUse ScratchSuccessExample
 */

/**
 * @api {get} /scratches/ Request all scratches
 * @apiName GetScratches
 * @apiGroup Scratches
 *
 * @apiUse ScratchesResult
 *
 * @apiUse ScratchesSuccessExample
 */