/**
 * @api {get} /scratchTypes/ Request Scratch types
 * @apiName GetScratchTypes
 * @apiGroup ScratchTypes
 *
 * @apiUse ScratchTypesResult
 *
 * @apiUse ScratchTypesSuccessExample
 */

/**
 * @api {post} /scratchTypes/ Create Scratch type
 * @apiName CreateScratchType
 * @apiGroup ScratchTypes
 *
 * @apiUse CreateScratchTypeParams
 *
 * @apiUse ScratchTypeResult
 *
 * @apiUse ScratchTypeSuccessExample
 */

/**
 * @api {get} /scratchTypes/:id Request Scratch type by id
 * @apiName GetScratchTypeById
 * @apiGroup ScratchTypes
 *
 * @apiParam {Number} id Scratch type's unique ID.
 *
 * @apiUse ScratchTypeResult
 *
 * @apiUse ScratchTypeSuccessExample
 */

/**
 * @api {post} /scratchTypes/:id Edit Scratch type
 * @apiName EditScratchType
 * @apiGroup ScratchTypes
 * @apiDescription Returns edited Scratch type
 *
 * @apiUse EditScratchTypeParams
 *
 * @apiUse ScratchTypeResult
 *
 * @apiUse ScratchTypeSuccessExample
 */

/**
 * @api {delete} /scratchTypes/:id Remove Scratch type
 * @apiName RemoveScratchType
 * @apiGroup ScratchTypes
 * @apiDescription Returns removed Scratch type
 *
 * @apiParam {Number} id Scratch type's unique ID.
 *
 * @apiUse ScratchTypeResult
 *
 * @apiUse ScratchTypeSuccessExample
 */

/**
 * @api {get} /scratchTypes/:id/scratches Request Scratches with selected typeId
 * @apiName GetScratchesByTypeId
 * @apiGroup ScratchTypes
 * @apiDescription Returns Scratches with selected typeId
 *
 * @apiParam {Number} id Scratch type's unique ID.
 *
 * @apiUse ScratchesResult
 *
 * @apiUse ScratchesSuccessExample
 */
