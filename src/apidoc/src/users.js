/**
 * @api {get} /users/:id/scratches Request User scratches
 * @apiName GetUserScratches
 * @apiGroup Users
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiUse ScratchesResult
 *
 * @apiUse ScratchesSuccessExample
 */

/**
 * @api {post} /users/:id/assignScratch/:typeId Assign scratch with certain type to user
 * @apiName AssignScratchToUser
 * @apiGroup Users
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} typeId Scratch type unique ID.
 *
 * @apiUse ScratchResult
 *
 * @apiUse ScratchSuccessExample
 */