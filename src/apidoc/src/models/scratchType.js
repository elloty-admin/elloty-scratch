/**
 * @apiDefine ScratchTypeResult
 * @apiSuccess {ScratchType}  scratchType        Scratch type.
 * @apiSuccess {Number}  scratchType.id   id
 * @apiSuccess {Number}  scratchType.win_coef coefficient of win
 * @apiSuccess {Number}  scratchType.price price of one scratch of this type
 * @apiSuccess {Boolean}  scratchType.enabled
 */

/**
 * @apiDefine ScratchTypesResult
 * @apiSuccess {ScratchTypes[]}  scratchTypes       Scratch type.
 * @apiSuccess {Number}  scratchTypes.id   id
 * @apiSuccess {Number}  scratchTypes.win_coef coefficient of win
 * @apiSuccess {Number}  scratchTypes.price price of one scratch of this type
 * @apiSuccess {Boolean}  scratchTypes.enabled
 */

/**
 * @apiDefine ScratchTypesSuccessExample
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "win_coef": 20,
 *         "price": 2,
 *         "enabled": true,
 *       },
 *       {
 *         "id": 2,
 *         "win_coef": 30,
 *         "price": 5,
 *         "enabled": false,
 *       }
 *     ]
 */

/**
 * @apiDefine ScratchTypeSuccessExample
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": 2,
 *       "win_coef": 30,
 *       "price": 5,
 *       "enabled": false,
 *     }
 */

/**
 * @apiDefine CreateScratchTypeParams
 * @apiParam {Number} win_coef
 * @apiParam {Number} price
 * @apiParam {Boolean} [enabled]
 */

/**
 * @apiDefine EditScratchTypeParams
 * @apiParam {Number} [win_coef]
 * @apiParam {Number} [price]
 * @apiParam {Boolean} [enabled]
 */