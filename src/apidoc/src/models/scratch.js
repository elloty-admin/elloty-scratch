/**
 * @apiDefine ScratchResult
 * @apiSuccess {Scratch}  scratch       User profile information.
 * @apiSuccess {Number}  scratch.id   id
 * @apiSuccess {Number}  scratch.type_id ScratchType id
 * @apiSuccess {Number}  scratch.user_id User id
 */

/**
 * @apiDefine ScratchesResult
 * @apiSuccess {Scratch[]}  scratches       User profile information.
 * @apiSuccess {Number}  scratches.id   id
 * @apiSuccess {Number}  scratches.type_id ScratchType id
 * @apiSuccess {Number}  scratches.user_id User id
 */

/**
 * @apiDefine ScratchesSuccessExample
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "id": 1,
 *         "type_id": 0,
 *         "user_id": 0,
 *         "prize": 200
 *       },
 *       {
 *         "id": 2,
 *         "type_id": 0,
 *         "user_id": 0,
 *         "prize": 50
 *       }
 *     ]
 */

/**
 * @apiDefine ScratchSuccessExample
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
*      {
*        "id": 1,
*        "type_id": 0,
*        "user_id": 0,
*        "prize": 200
*      }
 */