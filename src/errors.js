function Errors() {}

Errors.prototype = {
    ServiceError: function(msg) {
        var error = Error(msg);
        error.msg = msg;
        error.status = 500;
        return error;
    },

    NotFoundError: function(msg) {
        var error = Error(msg);
        error.msg = msg;
        error.status = 404;
        return error;
    }
};

module.exports = new Errors();