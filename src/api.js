const express = require('express');
const scratchController = require('./controllers/scratchController');
const scratchTypesController = require('./controllers/scratchTypesController');
const usersController = require('./controllers/usersController');
const router = express.Router();
const bodyParser = require('body-parser');
const config = require('./config/config');

router.use(bodyParser.json()); // support json encoded bodies
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

router.use("/scratches", scratchController);
router.use("/scratchTypes", scratchTypesController);
router.use("/users", usersController);

router.use(function(err, req, res, next) {
    if (err.status !== undefined) {
        res.status(err.status);
    } else {
        res.status(500);
    }
    if (config.env === config.dev) {
        err.stackTrace = err.stack;
    }
    res.send(err);
});

module.exports = router;