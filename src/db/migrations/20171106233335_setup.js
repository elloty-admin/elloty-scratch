
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists('scratch_types', function(table){
            table.bigincrements('id').primary();
            table.biginteger('price');
            table.decimal('win_coef');
            table.boolean('enabled');
        }),

        knex.schema.createTableIfNotExists('scratches', function(table){
            table.bigincrements('id').primary();
            table.biginteger('type_id').unsigned();
            table.foreign('type_id').references('scratch_types.id');
            table.biginteger('user_id').unsigned();
            table.biginteger('prize').unsigned();
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('scratches'),
        knex.schema.dropTableIfExists('scratch_types')
    ])
};
