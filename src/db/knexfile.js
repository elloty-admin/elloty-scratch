// Update with your config settings.
var config = require('../config/config');

var conf = {
    client: 'pg',
    connection: {
        host: config.db.host,
        database: config.db.name,
        user: config.db.user,
        password: config.db.password
    },
    pool: {
        min: 2,
        max: 10
    },
    migrations: {
        tableName: 'knex_migrations'
    }
};

module.exports = {
    development: conf,
    staging: conf,
    production: conf
};
