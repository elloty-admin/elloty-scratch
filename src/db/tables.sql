BEGIN;
CREATE TABLE IF NOT EXISTS public.scratch_types (
	id SERIAL NOT NULL,
	price bigint DEFAULT 0 NOT NULL,
	win_coef numeric DEFAULT 0 NOT NULL,
	enabled boolean DEFAULT false NOT NULL,
  PRIMARY KEY(id)
);
COMMIT;

BEGIN;
CREATE TABLE IF NOT EXISTS public.scratches (
  id SERIAL NOT NULL,
  type_id bigint NOT NULL,
  user_id bigint,
  prize numeric NOT NULL,
  PRIMARY KEY(id)
);
COMMIT;