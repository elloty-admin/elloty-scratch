var config = require('../config/config');
var knexFile = require('./knexfile');
const knex = require('knex')(knexFile[config.env]);

module.exports = knex;