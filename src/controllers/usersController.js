/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
const scratchService = require('../services/scratchService');
const express = require('express');
const router = express.Router();

const PARAM_ID = "id";
const PARAM_TYPE_ID = "typeId";

// "/api/uses/:id/assignScratch/:scratchTypeId"
router.post(`/:${PARAM_ID}/assignScratch/:${PARAM_TYPE_ID}`, function (req, res, next) {
    scratchService.assignScratchToUser(+req.params[PARAM_ID], +req.params[PARAM_TYPE_ID])
        .then(function (scratch) {
            res.send(scratch);
        })
        .catch(function (e) {
            next(e);
        });
});

// "/api/uses/:id/scratches/"
router.get(`/:${PARAM_ID}/scratches/`, function (req, res, next) {
    scratchService.getScratchesByUserId(+req.params[PARAM_ID])
    .then(function(scratches) {
            res.send(scratches);
        })
    .catch(function(e) {
        next(e);
    });
});

module.exports = router;