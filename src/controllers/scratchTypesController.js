/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
const scratchTypeService = require('../services/scratchTypeService');
const scratchService = require('../services/scratchService');
const express = require('express');
const router = express.Router();
const ScratchType = require('../models/scratchType');

const PARAM_WIN_COEFFICIENT = "winCoefficient";
const PARAM_ENABLED = "enabled";
const PARAM_ID = "id";
const PARAM_PRICE = "price";

router.route('/')
    // "/api/scratchTypes/"
    .get(function (req, res, next) {
        scratchTypeService.getScratchTypes().then(function(scratchTypes) {
            res.send(scratchTypes);
        })
        .catch(function(e) {
            next(e);
        });
    })

    // "/api/scratchTypes/"
    .post(function (req, res, next) {
        var scratchType = new ScratchType();
        scratchType.setWinCoefficient(+req.body[PARAM_WIN_COEFFICIENT]);
        scratchType.setPrice(+req.body[PARAM_PRICE]);
        scratchType.setEnabled(req.body[PARAM_ENABLED]);
        scratchTypeService.createNewScratchType(scratchType).then(function(data) {
            res.send(data);
        })
        .catch(function(e) {
            next(e);
        });
    });

router.route(`/:${PARAM_ID}`)
    // "/api/scratchTypes/:id"
    .get(function (req, res, next) {
        scratchTypeService.getScratchTypeById(+req.params[PARAM_ID]).then(function(scratchType) {
            res.send(scratchType);
        })
        .catch(function(e) {
            next(e);
        });
    })

    // "/api/scratchTypes/:id"
    .post(function (req, res, next) {
        var updatedScratchType = new ScratchType();
        if (req.body[PARAM_WIN_COEFFICIENT]) {
            updatedScratchType.setWinCoefficient(+req.body[PARAM_WIN_COEFFICIENT]);
        }
        if (req.body[PARAM_PRICE]) {
            updatedScratchType.setPrice(+req.body[PARAM_PRICE]);
        }
        if (req.body[PARAM_ENABLED]) {
            updatedScratchType.setEnabled(req.body[PARAM_ENABLED]);
        }
        var id = +req.params[PARAM_ID];
        updatedScratchType.setId(id);
        scratchTypeService.editScratchType(updatedScratchType).then(function(data) {
            res.send(data);
        })
        .catch(function(e) {
            next(e);
        });
    })

    // "/api/scratchTypes/:id"
    .delete(function (req, res, next) {
        scratchTypeService.removeScratchType(+req.params[PARAM_ID]).then(function(scratchType) {
            res.send(scratchType);
        })
        .catch(function(e) {
            next(e);
        });
    });

router.route(`/:${PARAM_ID}/scratches`)
    // "/api/scratchTypes/:id/scratches"
    .get(function (req, res, next) {
        scratchService.getScratchesByTypeId(+req.params[PARAM_ID])
        .then(function(scratches) {
            res.send(scratches);
        })
        .catch(function(e) {
            next(e);
        });
    });

module.exports = router;