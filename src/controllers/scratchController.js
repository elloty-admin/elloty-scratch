/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
const scratchService = require('../services/scratchService');
const express = require('express');
const router = express.Router();
const Scratch = require('../models/scratch');
const Errors = require('../errors');

const PARAM_ID = "id";
const PARAM_TYPE_ID = "typeId";
const PARAM_PRIZE = "prize";

router.route(`/:${PARAM_ID}`)
    // "/api/scratches/:id"
    .get(function (req, res, next) {
        scratchService.getScratchById(+req.params[PARAM_ID])
            .then(function (scratch) {
                res.send(scratch);
            })
            .catch(function (e) {
                next(e);
            });
    })

    // "/api/scratches/:id"
    .post(function (req, res) {
        //var scratch = new Scratch();
        //if (req.body[PARAM_TYPE_ID]) {
        //    scratch.setTypeId(+req.body[PARAM_TYPE_ID]);
        //}
        //if (req.body[PARAM_PRIZE]) {
        //    scratch.setPrize(+req.body[PARAM_PRIZE]);
        //}
        //scratch.setId(+req.params[PARAM_ID]);
        //scratch = scratchService.editScratch(scratch);
        //res.send(scratch);
        throw Errors.ServiceError("Unsupported operation");
    })

    // "/api/scratches/:id"
    .delete(function (req, res) {
        //var scratch = scratchService.removeScratch(+req.params[PARAM_ID]);
        //res.send(scratch);
        throw Errors.ServiceError("Unsupported operation");
    });

router.route('/')
    // "/api/scratches/"
    .get(function (req, res, next) {
        scratchService.getScratches()
            .then(function (scratches) {
                res.send(scratches);
            })
            .catch(function (e) {
                next(e);
            })
    });

module.exports = router;