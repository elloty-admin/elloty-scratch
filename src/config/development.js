var config = {
    logging: true,

    db: {
        host: process.env.POSTGRES_HOST,
        name: process.env.POSTGRES_DB,
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD
    }
};

module.exports = config;
