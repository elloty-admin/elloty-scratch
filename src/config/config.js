var config = {
    dev: "development",
    prod: "production",

    port: process.env.API_PORT,
    scratchCountToCreate: 100,
    logging: false
};

config.env = process.env.NODE_ENV || config.dev;
process.env.NODE_ENV = config.env;

var envConfig;
try {
    envConfig = require('./' + config.env) || {};
} catch(e) {
    envConfig = {};
}
module.exports = Object.assign({}, config, envConfig);