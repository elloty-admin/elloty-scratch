/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
const TABLE_NAME = 'scratches';
var Scratch = require('../models/scratch');
const scratchTypeService = require('./scratchTypeService');
const Errors = require('../errors');
var knex = require('./../db/knexWrapper');
const scratchFactory = require('./scratchFactory');

class ScratchService {
    assignScratchToUser(userId, typeId) {
        return scratchTypeService.getScratchTypeById(typeId)
            .then(function (scratchType) {
                if (scratchType) {
                    return knex.transaction(function (trx) {
                        return knex
                            .select("id")
                            .from(TABLE_NAME)
                            .whereNull('user_id')
                            .andWhere({type_id: typeId})
                            .limit(1)
                            .transacting(trx)
                            .then(function (ids) {
                                if (ids.length == 1) {
                                    return knex(TABLE_NAME)
                                        .returning("*")
                                        .where({id: ids[0].id})
                                        .update({user_id: userId})
                                        .transacting(trx)
                                        .then(function (scratches) {
                                            return Scratch.parseDbObject(scratches[0]);
                                        })
                                } else {
                                    return knex(TABLE_NAME)
                                        .returning("*")
                                        .insert(scratchFactory.createScratches(scratchType))
                                        .transacting(trx)
                                        .then(function () {
                                            return knex.select("id")
                                                .from(TABLE_NAME)
                                                .whereNull('user_id')
                                                .andWhere({type_id: typeId})
                                                .limit(1)
                                                .transacting(trx)
                                                .then(function (ids) {
                                                    return knex(TABLE_NAME)
                                                        .returning("*")
                                                        .where({id: ids[0].id})
                                                        .update({user_id: userId})
                                                        .transacting(trx);
                                                })
                                                .then(function (scratches) {
                                                    return Scratch.parseDbObject(scratches[0]);
                                                })
                                        })
                                }
                            })
                            .then(trx.commit)
                            .catch(function(e) {
                                trx.rollback(e);
                                console.error(e);
                                throw Errors.ServiceError("Service unavailable");
                            })
                    })
                } else {
                    throw Errors.NotFoundError(`Scratch type with id [${typeId}] not found`);
                }
            })
    }

    getScratchesByUserId(userId) {
        return knex
            .select('*')
            .from((TABLE_NAME))
            .where({user_id: userId})
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i] = Scratch.parseDbObject(data[i]);
                }
                return data;
            })
    }

    getScratchesByTypeId(typeId) {
        return knex
            .select('*')
            .from((TABLE_NAME))
            .where({type_id: typeId})
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i] = Scratch.parseDbObject(data[i]);
                }
                return data;
            })
    }

    getScratchById(scratchId) {
        return knex
            .select('*')
            .where({id: scratchId})
            .from((TABLE_NAME))
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (scratches) {
                if (!scratches || scratches.length == 0) {
                    throw Errors.NotFoundError(`Scratch with id [${scratchId}] not found`);
                } else {
                    return Scratch.parseDbObject(scratches[0]);
                }
            })
    }

    removeScratch(scratchId) {
        return knex(TABLE_NAME)
            .where({id: scratchId})
            .del()
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function () {
                return(scratchId);
            })
    }

    getScratches() {
        return knex
            .select('*')
            .from((TABLE_NAME))
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i] = Scratch.parseDbObject(data[i]);
                }
                return data;
            })
    }
}

module.exports = ScratchService;