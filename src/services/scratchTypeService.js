/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
var scratchTypeService = require('./scratchTypeServiceImpl');

class ScratchTypeService {
    createNewScratchType(scratchType) {
    }

    getScratchTypes() {
    }

    getScratchTypeById(typeId) {
    }

    editScratchType(updatedScratchType) {
    }

    removeScratchType(typeId) {
    }
}

module.exports = new scratchTypeService();