/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
const TABLE_NAME = 'scratch_types';
var ScratchType = require('../models/scratchType');
const Errors = require('../errors');
var knex = require('./../db/knexWrapper');

class ScratchTypeService {
    createNewScratchType(scratchType) {
        return knex(TABLE_NAME)
            .returning('*')
            .insert({price: scratchType.price, win_coef: scratchType.winCoefficient, enabled: scratchType.enabled})
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                return ScratchType.parseDbObj(data[0]);
            })
    }

    getScratchTypes() {
        return knex
            .select('*')
            .from((TABLE_NAME))
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    data[i] = ScratchType.parseDbObj(data[i]);
                }
                return data;
            })
    }

    getScratchTypeById(typeId) {
        return knex
            .select('*')
            .where({id: typeId})
            .from((TABLE_NAME))
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (scratchTypes) {
                if (!scratchTypes || scratchTypes.length == 0) {
                    throw Errors.NotFoundError(`Scratch type with id [${typeId}] not found`);
                } else {
                    return ScratchType.parseDbObj(scratchTypes[0]);
                }
            })
    }

    editScratchType(updatedScratchType) {
        return knex(TABLE_NAME)
            .returning('*')
            .where({id: updatedScratchType.id})
            .update(ScratchType.getDbObject(updatedScratchType))
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (data) {
                if (!data || data.length == 0) {
                    throw Errors.NotFoundError(`Scratch type with id [${updatedScratchType.id}] not found`);
                } else {
                    return ScratchType.parseDbObj(data[0]);
                }
            })
    }

    removeScratchType(typeId) {
        return knex(TABLE_NAME)
            .returning('*')
            .where({id: typeId})
            .del()
            .catch(function(e) {
                console.error(e);
                throw Errors.ServiceError("Service unavailable");
            })
            .then(function (result) {
                if (!result || result.length == 0) {
                    throw Errors.NotFoundError(`Scratch type with id [${typeId}] not found`);
                } else {
                    return ScratchType.parseDbObj(result[0]);
                }
            })
    }
}

module.exports = ScratchTypeService;