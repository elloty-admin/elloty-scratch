/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
var scratchService = require('./scratchServiceImpl');

class ScratchService {
    assignScratchToUser(userId, typeId) {
    }

    createScratch(typeId) {
    }

    getScratchesByUserId(userId) {
    }

    getScratchesByTypeId(typeId) {
    }

    getScratchById(scratchId) {
    }

    editScratch(updatedScratch) {
    }

    removeScratch(scratchId) {
    }

    getScratches() {
    }
}

module.exports = new scratchService();