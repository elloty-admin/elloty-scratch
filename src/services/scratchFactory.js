var config = require('../config/config');
var Scratch = require('../models/scratch');

function Factory() {}

Factory.prototype = {
    createScratches(scratchType) {
        var scratches = [];
        var price = +scratchType.price;
        var countToCreate = config.scratchCountToCreate;
        var winCoef = +scratchType.winCoefficient;
        var sum = price * countToCreate * winCoef / 100;
        var x = sum / 1.644;
        for (var i = 1; i <= countToCreate; i++) {
            var scratch = new Scratch();
            scratch.prize = Math.floor(x / (i * i));
            scratch.typeId = scratchType.id;
            scratches.push(Scratch.getDbObject(scratch));
        }
        return scratches;
    }
};

module.exports = new Factory();