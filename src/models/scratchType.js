/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
class ScratchType {
    constructor() {
    }

    setId(id) {
        this.id = id;
    }

    setPrice(price) {
        this.price = price;
    }

    setEnabled(enabled) {
        this.enabled = enabled;
    }

    setWinCoefficient(winCoefficient) {
        this.winCoefficient = winCoefficient;
    }

    static parseDbObj(obj) {
        var scratchType = new ScratchType();
        scratchType.id = +obj.id;
        scratchType.enabled = obj.enabled;
        scratchType.price = +obj.price;
        scratchType.winCoefficient = +obj.win_coef;
        return scratchType;
    }

    static getDbObject(bean) {
        var obj = Object.assign({}, bean);
        obj.win_coef = obj.winCoefficient;
        delete obj.winCoefficient;
        return obj;
    }
}

module.exports = ScratchType;