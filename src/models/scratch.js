/**
 * Created by konstantin.bogdanov on 21/10/2017.
 */
class Scratch {
    constructor() {
    }

    setUserId(userId) {
        this.userId = userId;
    }

    setPrize(prize) {
        this.prize = prize;
    }

    setTypeId(typeId) {
        this.typeId = typeId;
    }

    setId(id) {
        this.id = id;
    }

    static parseDbObject(obj) {
        var scratch = new Scratch();
        scratch.setId(+obj.id);
        scratch.setTypeId(+obj.type_id);
        scratch.setPrize(+obj.prize);
        scratch.setUserId(+obj.user_id || undefined);
        return scratch;
    }

    static getDbObject(bean) {
        var obj = Object.assign({}, bean);
        obj.type_id = obj.typeId;
        delete obj.typeId;
        obj.user_id = obj.userId;
        delete obj.userId;
        return obj;
    }
}

module.exports = Scratch;