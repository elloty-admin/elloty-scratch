const express = require('express');
const api = require('./api');
const app = express();
const config = require('./config/config');

app.listen(config.port, function () {
    console.log(`App is listening on port ${config.port}!`);
});

app.use('/api', api);
app.use('/apidocs', express.static('apidoc/output'));