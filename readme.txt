To start:
- create .env file in project root
- run "docker-compose up"

Endpoints:
- /api/
- /apidocs/

TODO:
- logging
- inputs validation
- error handling
- documentation
- paging